/** File server.c
 * @author Thomas Lextrait
 */

#include "server.h"

#ifndef INADDR_NONE
#define INADDR_NONE 0xffffffff /* should be in <netinet/in.h> */
#endif

#ifndef STDIN
#define STDIN 0
#endif

#ifndef STDOUT
#define STDOUT 1
#endif

#ifndef STDERR
#define STDERR 2
#endif

/**
* GLOBAL VARIABLES
*/
int verbose = -1;
int specPort = -1;
int specPass = -1;
int skipProc = -1;
int big = -1;

int main(int argc, char *argv[])
{
	/* ------------------------------------------------------------------------ */
	/*							VAR DECLARATIONS								*/
	/* ------------------------------------------------------------------------ */
	
	// Option stuff
	int c;
	extern int optind, opterr;
	extern char *optarg;
	char* password = (char*)malloc(sizeof(char)*MAX_COMMAND_LEN);
	
	// Socket stuff
	char* message = (char*)malloc(sizeof(char)*MSG_SIZE);
	int bytes;
	int sock, newsock, tcp_port;
	unsigned int clilen;
	struct sockaddr_in cli_addr, serv_addr;
	
	// Process stuff
	int pid;
	
	// Data reading
	char** clientArgs = (char**)malloc(sizeof(char*)*MAX_ARGS);
	int receivedArgs, i, a;
	
	// initialize client args
	for(i=0; i<MAX_ARGS; i++){
		clientArgs[i] = (char*)malloc(sizeof(char)*MAX_COMMAND_LEN);
		memset(clientArgs[i], '\0', MAX_COMMAND_LEN);
	}
	
	/* ------------------------------------------------------------------------ */

	// Parse command line options
	while((c = getopt (argc, argv, "hsvbp:l:")) != EOF) {
	    switch(c){
			case 's': // skip processing
				skipProc = 0;
				break;
			case 'v': // verbose
				verbose = 0;
				break;
			case 'p': // specify port
				tcp_port = atoi(optarg);
				specPort = 0;
				break;
			case 'l': // specify login (password)
				strcpy(password, optarg);
				specPass = 0;
				break;
			case 'b': // big files
				big = 0;
				break;
			default:
		    case '?': // opt error
		    case 'h': // help
		      	help();
				exit(1);
	    }
  	}

	// DEFAULT Port
	if(specPort != 0){tcp_port = SERVER_PORT;}
	
	// Create socket from which to read
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
	   perror("Can't open stream socket.\n");
	   exit(1);
	}

	// bind our local address so client can connect to us
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(tcp_port);
	if (bind(sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		perror("Can't bind to local address.\n");
		exit(1);
	}

	printf("Server accepting connections.\n");

	listen(sock, MAX_CONNECTIONS); // mark the socket as passive, and sets backlog num 

	while(0==0){
		
		clilen = sizeof(cli_addr);
		newsock = accept(sock, (struct sockaddr *) &cli_addr, &clilen);
		if (newsock < 0) {
			perror("Error while accepting connection.\n");
			exit(1);
		}
		
		// Skipping processing? printing takes time
		if(skipProc != 0){printf("\nConnection request received.\n");}
		
		pid = fork();
		
		if(pid==-1){
			perror("fork() error\n");
			exit(0);
		}else if(pid==0){
			
			// CHILD PROCESS - SERVER
			if((bytes = read(newsock, message, MSG_SIZE)) < 0){
				perror("Error in read.\n");
				exit(0);
			}
			
			// Skip processing?
			if(skipProc!=0){
				
				// How many args?
				receivedArgs = 0;
				for(i=0; i < bytes; i++){
					if(message[i]=='\0'){receivedArgs++;}
				}

				if(verbose==0){printf("Received %d bytes, %d arguments.\n", bytes, receivedArgs-1);}

				// Parse received data into strings
				for(i=0; i<receivedArgs && i<MAX_ARGS; i++){
					strcpy(clientArgs[i], message);
					if(verbose==0){printf("%d: %s\n", i, clientArgs[i]);}
					a = 0;
					while(message[a]!='\0'){a++;}
					message+=a+1;
				}

				// Make other arguments null
				for(i=receivedArgs; i<MAX_ARGS; i++){
					clientArgs[i] = NULL;
				}

				// Check password
				if(
					strcmp(clientArgs[0], SERVER_PASS) == 0 ||
					strcmp(clientArgs[0], password) == 0
				){
					printf("Password correct.\n");

					// Send ack
					if(write(newsock, "passok\0", 7) == -1) {
						perror("Write failed.\n");
						exit(0);
					}
					
					if(big == 0){
						// Send a million bytes
						for(i=0; i<MILLION; i++){
							message[i] = rand()%100+100;
						}
						if(write(newsock, message, MILLION) == -1) {
							perror("Write failed.\n");
							exit(0);
						}
					}else{
						
						clientArgs++;

						printf("Command: %s", clientArgs[0]);
						for(i=1; i<receivedArgs-1; i++){
							printf(" %s", clientArgs[i]);
						}
						printf("\n");

						if(dup2(newsock, STDIN) < 0){
							perror("Failed to dup2 stdin.\n");
							exit(0);
						}
						if(dup2(newsock, STDOUT) < 0){
							perror("Failed to dup2 stdout.\n");
							exit(0);
						}
						if(dup2(newsock, STDERR) < 0){
							perror("Failed to dup2 stderr.\n");
							exit(0);
						}

						// Execute command
						execvp(clientArgs[0], clientArgs);
						
					}

				}else{
					printf("Password incorrect!\n");

					// Send ack
					if(write(newsock, "passntk\0", 8) == -1) {
						perror("Write failed.\n");
						exit(0);
					}
				}
				
			}else{
				// Send ack
				if(write(newsock, "done\0", 8) == -1) {
					perror("Write failed.\n");
					exit(0);
				}
			}
			
			close(newsock);
			close(sock);
			exit(0);
			
		}else{
			// PARENT PROCESS - SERVER
			while(wait(NULL) > 0){}
		}
		
		close(newsock);
	}
	
	exit(0);
}

/**
* Print a help message
*/
void help()
{
	printf("server - thomas.lextrait@gmail.com\n");
	printf("usage: server [-h|-v|-s|-b] [-p<port>] [-l<password>]\n");
	printf("\t-t\tSpecify a port, otherwise will use 6666.\n");
	printf("\t-l\tSpecify a password, otherwise will use 'hello'.\n");
	printf("\t-v\tBe verbose.\n");
	printf("\t-s\tSkip all processing and only perform the connection with clients. This removes password verification.\n");
	printf("\t-b\tInstead of executing commands, send 1 million bytes of data to clients.\n");
	printf("\t-h\tDisplay this help message.\n");
}

