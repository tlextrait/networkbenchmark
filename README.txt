CS4513 - Distributed Systems
Project 2

Thomas Lextrait
tlextrait@wpi.edu

Language:
C

Compilation:
Use make

Client:
Everything should be implemented according to specifications. Very occasionally the client hangs for an unknown reason, this may happen when the previous call to the server contained invalid arguments. Just quit the client with CTRL+C and try again if it happens, this should not affect the server.

client [-h|-v|-t] [-s<server ip>] [-p<port>] [-r<repeat>] password command...
	-s	Specify a server ip address.
	-p	Specify a port, otherwise will use 6666.
	-r	Repeat the request <repeat> number of times (server needs to be started in -s mode).
	-v	Be verbose.
	-t	Display total time elapsed.
	-h	Display this help message.

Example:
./client -v -t -s 192.168.1.110 -p 7676 hello more client.c

You should specify a server IP address, otherwise it will use 192.168.1.108 which is my laptop's ip on my local network. If you don't want to specify it every time, you may simply hard-code your IP in the macro in client.h.

The mode -r was specifically implemented to be used for experiment #1 and requires the server to be in mode -s, which allows skipping a lot of things. If the server is not run in -s mode but -r was specified for the client, the client will hang.

Server:
The server implements everything and has always ran smooth in all my tests.

server [-h|-v|-s|-b] [-p<port>] [-l<password>]
	-t	Specify a port, otherwise will use 6666.
	-l	Specify a password, otherwise will use 'hello'.
	-v	Be verbose.
	-s	Skip all processing and only perform the connection with clients. This removes password verification.
	-b	Instead of executing commands, send 1 million bytes of data to clients.
	-h	Display this help message.

Example:
./server -v -p 7676 -l hello

The default server password is "hello", but you may specify your own.
