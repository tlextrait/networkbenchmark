/** File client.c
 * @author Thomas Lextrait
 */

#include "client.h"

#ifndef INADDR_NONE
#define INADDR_NONE 0xffffffff /* should be in <netinet/in.h> */
#endif

/**
* GLOBAL VARIABLES
*/
int verbose = -1;
int specServer = -1;
int timeShow = -1;
int specPort = -1;

int main(int argc, char *argv[])
{
	/* ------------------------------------------------------------------------ */
	/*							VAR DECLARATIONS								*/
	/* ------------------------------------------------------------------------ */
	
	// Option stuff
	int c;
	extern int optind, opterr;
	extern char *optarg;
	
	// Timing
	struct timeval tv1, tv2;
	struct timezone tz;
	long totalTime=0;
	
	char *serv_host_addr = (char*)malloc(sizeof(char)*20);
	int tcp_port;
	int repeatTimes = 1;
	int i;
	
	
	/* ------------------------------------------------------------------------ */

	// Parse command line options
	while((c = getopt (argc, argv, "hvts:p:r:")) != EOF) {
	    switch(c){
			case 'v': // verbose
				verbose = 0;
				break;
			case 't': // display time
				timeShow = 0;
				break;
			case 's': // specify server ip
				serv_host_addr = optarg;
				specServer = 0;
				break;
			case 'p': // specify port
				tcp_port = atoi(optarg);
				specPort = 0;
				break;
			case 'r':
				repeatTimes = atoi(optarg);
				if(repeatTimes < 1){repeatTimes = 1;}
				break;
			default:
		    case '?': // opt error
		    case 'h': // help
		      	help();
				exit(1);
	    }
  	}

	if(argc < 3){
		help();
	}else{
		
		if(repeatTimes > 1){
			printf("Repeating %d times\n", repeatTimes);
		}
		
		// DEFAULT PORT/ADDRESS
		if(specPort != 0){tcp_port = SERVER_PORT;}
		if(specServer != 0){serv_host_addr = SERVER_ADDR;}
		
		if(timeShow==0){gettimeofday(&tv1, &tz);}
		
		for(i=0; i<repeatTimes; i++){
			executeClient(serv_host_addr, tcp_port, argc, argv);
		}
		
		if(timeShow==0){
			gettimeofday(&tv2, &tz);
			totalTime += (tv2.tv_sec - tv1.tv_sec) * 1000 + (tv2.tv_usec - tv1.tv_usec)/1000;
		}
		
	}
	
	if(timeShow == 0){
		printf("Total time:\t%li milliseconds\n", totalTime);
	}
	
	exit(0);
}

/**
* Executes the client program
*/
void executeClient(char* address, int port, int argc, char* argv[])
{
	
	// Socket stuff
	int sock, bytes, sendingCounter, totalBytes;
	struct sockaddr_in serv_addr;
	struct hostent *hp;
	char* message = (char*)malloc(sizeof(char)*BIG_MSG_SIZE);
	memset(message, '\0', BIG_MSG_SIZE);
	
	/* ------------------------------------------------------------------------ */

	if(verbose==0){printf("Trying to connect to server %s at port %d...\n", address, port);}

	/*
	 * First try to convert the host name as a dotted-decimal number.
	 * Only if that fails do we call gethostbyname().
	 */
	bzero((void *) &serv_addr, sizeof(serv_addr));

	if(verbose==0){printf("Looking up %s...\n", address);}

	if ((hp = gethostbyname(address)) == NULL) {
	  perror("Host name error.\n");
	  exit(1);
	}
	
	bcopy(hp->h_addr, (char *) &serv_addr.sin_addr, hp->h_length);
	serv_addr.sin_family = AF_INET;
	
	if(verbose == 0){printf("Server found. Setting port connection to %d...\n", port);}
	serv_addr.sin_port = htons(port);
	
	if(verbose == 0){printf("Done. Opening socket...\n");}
	
	/* open a TCP socket (an Internet stream socket). */
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		perror("Can't open stream socket.\n");
		exit(1);
	}
	
	if(verbose == 0){printf("Open. Trying connection to server...\n");}
	
	/* socket open, so connect to the server */
	if (connect(sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		perror("Can't connect.\n");
		exit(1);
	}

	if(verbose == 0){printf("Connection established!\n");}
	
	/* Concatenate the strings */
	totalBytes = 0;
	for(sendingCounter=optind; sendingCounter<argc; sendingCounter++){
		message = strcat(message, argv[sendingCounter]);
		totalBytes += strlen(message)+1;
		message += strlen(message)+1;
	}
	message -= totalBytes;
	
	if(write(sock, message, totalBytes) == -1) {
		perror("Sending commands failed.\n");
	}
	
	/* Wait for password ACK */
	bytes = read(sock, message, BIG_MSG_SIZE);
	if(strcmp(message, "passok")==0){
		printf("Password accepted.\n");
		printf("---------------------------------------------------------------------------\n");
		bytes = read(sock, message, BIG_MSG_SIZE);
		printf("%s", message);
		printf("---------------------------------------------------------------------------\n");
	}else if(strcmp(message, "done")!=0){
		printf("Password was incorrect!\n");
	}
	
	if(verbose == 0){printf("Done sending data, closing connection...\n");}

	close(sock);
}

/**
* Print a help message
*/
void help()
{
	printf("client - thomas.lextrait@gmail.com\n");
	printf("usage: client [-h|-v|-t] [-s<server ip>] [-p<port>] [-r<repeat>] password command...\n");
	printf("\t-s\tSpecify a server ip address.\n");
	printf("\t-p\tSpecify a port, otherwise will use 6666.\n");
	printf("\t-r\tRepeat the request <repeat> number of times (server needs to be started in -s mode).\n");
	printf("\t-v\tBe verbose.\n");
	printf("\t-t\tDisplay total time elapsed.\n");
	printf("\t-h\tDisplay this help message.\n");
}

