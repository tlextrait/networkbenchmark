/**
* @file server.h
* @author Thomas Lextrait
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <string.h>

#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define SERVER_PORT 	6666
#define SERVER_ADDR		"192.168.1.108"
#define MAX_CONNECTIONS 500
#define MSG_SIZE 		10240		// 10KB
#define BIG_MSG_SIZE	1610612736	// 1.5GB
#define MILLION			1000000
#define SERVER_PASS		"hello"
#define MAX_ARGS		32
#define MAX_COMMAND_LEN 256

/**
* Print a help message
*/
void help();
