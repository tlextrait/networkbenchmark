/**
* @file client.h
* @author Thomas Lextrait
*/

#include <stdio.h>		// remove()
#include <stdlib.h>
#include <unistd.h>		// getopt()
#include <sys/time.h>
#include <string.h>

#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define SERVER_PORT 	6666
#define SERVER_ADDR		"192.168.1.108"
#define MSG_SIZE 		10240		// 10KB
#define BIG_MSG_SIZE	1000100
#define MILLION			1000000
#define MAX_ARGS		32
#define MAX_COMMAND_LEN 256

/**
* Executes the client program
*/
void executeClient(char* address, int port, int argc, char* argv[]);

/**
* Print a help message
*/
void help();
